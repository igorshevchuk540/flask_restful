# Final Project for Epam Python course.

### This is Second part of My Final Project
[First part](https://gitlab.com/igorshevchuk540/Epam_final_project) (Web-api)<br>
In this app I used Flask-RESTful.
<br>
### Documentation:
To use my app you can download it from my gitlab repository, then run the following command: ```pip install -r requirements.txt```
to install dependencies, needed to start the app(File is also in the working directory). After that simply run fule ```app.py```.
##### Below is given structure of my application.
1. In folder ```/migrations``` lie migration files for *DB*, used in this project.
2. ```App.py``` is main file of my app.
3. ```dependencies.txt``` contains all packages, needed to run the app.
4. ```test.py``` has Unit tests to test the app.

In this app i created CRUD methods for two resources:
1. Departments
2. Employees

Resources can be accessed by specific urls, starting with ```localhost/api/```.