from app import app
import unittest
import json


class FlaskTest(unittest.TestCase):

    # Test post method to add department
    def test_add_department(self):
        # Given
        name = "Test Department"
        address = "Test Address"
        payload = json.dumps({
            "name": name,
            "address": address
        })
        tester = app.test_client(self)
        response = tester.post('/api/departments/', headers={"Content-Type": "application/json"}, data=payload)
        self.assertEqual(204, response.status_code)

    # Test get method for first department
    def test_get_one_dep(self):
        tester = app.test_client(self)
        response = tester.get('/api/department/1')
        self.assertEqual(response.status_code, 200)

    # Test get method for List of departments
    def test_get_many_dep(self):
        tester = app.test_client(self)
        response = tester.get('/api/departments/')
        self.assertEqual(response.status_code, 200)

    ##################################

    # Test post method to add employee
    def test_add_employee(self):
        # Given
        name = "Test Employee"
        date_of_birth = "2000-01-01"
        salary = "1000"
        department_id = "1"
        payload = json.dumps({
            "name": name,
            "date_of_birth": date_of_birth,
            "salary": salary,
            "department_id": department_id
        })
        tester = app.test_client(self)
        response = tester.post('/api/employees/', headers={"Content-Type": "application/json"}, data=payload)
        self.assertEqual(204, response.status_code)

    # Test get method for first employee
    def test_get_one_emp(self):
        tester = app.test_client(self)
        response = tester.get('/api/employee/1')
        self.assertEqual(response.status_code, 200)

    # Test get method for List of employees
    def test_get_many_emp(self):
        tester = app.test_client(self)
        response = tester.get('/api/employees/')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
