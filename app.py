from flask import Flask, jsonify, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
import pymysql

'''Configure app'''
app = Flask(__name__, template_folder='templates')
app.secret_key = 'igor'
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:igor1234@localhost/test_rest'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)


class Department(db.Model):
    """Class to represent Department model"""
    __tablename__ = 'department'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(50))
    db.relationship('Employee', backref='employee', lazy=True)

    @property
    def serialize(self):
        """Return object data in easily serializable format"""
        return {
            'id': self.id,
            'modified_at': self.address
        }

    def __init__(self, name, address):
        self.name = name
        self.address = address


class Employee(db.Model):
    """Class to represent Employee model"""
    __tablename__ = 'employee'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    salary = db.Column(db.Float, nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'),
                              nullable=False)

    def __init__(self, name, date_of_birth, salary, department_id):
        self.name = name
        self.date_of_birth = date_of_birth
        self.salary = salary
        self.department_id = department_id


class PostDepartment(ma.Schema):
    """Marshmellow Class for Departments to define output format"""

    class Meta:
        fields = ('id', 'name', 'address')
        model = Department


'''Argument parser for Department'''
post_department = PostDepartment()
post_departments = PostDepartment(many=True)

department_args = reqparse.RequestParser()
department_args.add_argument("name", type=str, help="Name of the Department")
department_args.add_argument("address", type=str, help="Address of the Department")


class PostEmployee(ma.Schema):
    """Marshmellow Class for Emploees to define output format"""

    class Meta:
        fields = ('id', 'name', 'date_of_birth', 'salary', 'department_id')
        model = Employee


'''Argument parser for Employee'''
post_employee = PostEmployee()
post_employees = PostEmployee(many=True)

employee_args = reqparse.RequestParser()
employee_args.add_argument("name", type=str, help="Name of the Department")
employee_args.add_argument("date_of_birth", type=str, help="Name of the Department")
employee_args.add_argument("salary", type=str, help="Address of the Department")
employee_args.add_argument("department_id", type=int, help="Address of the Department")


# Default route
@app.route('/')
def hello():
    return 'Hello'


# Create api for Departments
class SingleDepartment(Resource):
    """Class to make actions with single Department.
        It allows to GET, EDIT(patch) and delete Department"""

    def get(self, dep_id):
        result = Department.query.filter_by(id=dep_id).first()
        if not result:
            abort(404, message="Could not find video with that id")
        return post_department.dump(result)

    def patch(self, dep_id):
        args = department_args.parse_args()
        result = Department.query.filter_by(id=dep_id).first()
        if not result:
            abort(409, message="Department does not exist...")
        result.name = args['name']
        result.address = args['address']
        db.session.commit()
        return post_department.dump(result), 204

    def delete(self, dep_id):
        dep = Department.query.get_or_404(dep_id)
        db.session.delete(dep)
        db.session.commit()
        return '', 204


class ListDepartments(Resource):
    """Class to make actions with multiple Departments.
        It allows to GET list of Departments and add new Department"""

    def get(self):
        deps = Department.query.all()
        return post_departments.dump(deps)

    # new
    def post(self):
        args = department_args.parse_args()
        new_dep = Department(
            name=args['name'],
            address=args['address']
        )
        db.session.add(new_dep)
        db.session.commit()
        dep = Department.query.filter_by(name=args['name']).all()

        return post_department.dump(dep), 204


# Create api for Employees
class SingleEmployee(Resource):
    """Class to make actions with single Employee.
        It allows to GET, EDIT(patch) and delete Employee"""
    def get(self, emp_id):
        result = Employee.query.filter_by(id=emp_id).first()
        if not result:
            abort(404, message="Could not find video with that id")
        return post_employee.dump(result)

    def patch(self, emp_id):
        args = employee_args.parse_args()
        result = Employee.query.filter_by(id=emp_id).first()
        if not result:
            abort(409, message="Department does not exist...")
        result.name = args['name']
        result.date_of_birth = args['date_of_birth']
        result.salary = args['salary']
        result.department_id = args['department_id']
        db.session.commit()
        return post_employee.dump(result), 204

    def delete(self, emp_id):
        emp = Employee.query.get_or_404(emp_id)
        db.session.delete(emp)
        db.session.commit()
        return '', 204


class ListEmployees(Resource):
    """Class to make actions with multiple Employees.
        It allows to GET list of Employees and add new Employee"""

    def get(self):
        emps = Employee.query.all()
        return post_employees.dump(emps)

    # new
    def post(self):
        args = employee_args.parse_args()
        new_emp = Employee(
            name=args['name'],
            date_of_birth=args['date_of_birth'],
            salary=args['salary'],
            department_id=args['department_id']
        )
        db.session.add(new_emp)
        db.session.commit()
        dep = Employee.query.filter_by(name=args['name']).first()

        return post_employee.dump(dep), 204


# Add resources for Departments
api.add_resource(SingleDepartment, "/api/department/<int:dep_id>")
api.add_resource(ListDepartments, '/api/departments/')

# Add resources for Employees
api.add_resource(SingleEmployee, "/api/employee/<int:emp_id>")
api.add_resource(ListEmployees, '/api/employees/')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
