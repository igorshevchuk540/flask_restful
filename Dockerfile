FROM python:3.8
COPY ./requirements.txt /restful_api/requirements.txt

WORKDIR /restful_api

RUN pip install -r requirements.txt

COPY . /restful_api

ENTRYPOINT python app.py